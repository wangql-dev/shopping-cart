class CartsController < ApplicationController
  protect_from_forgery
  before_action :authenticate_user!

  def create
    product_id = params[:product_id].to_i
    product = Product.find_by(id: product_id)
    if product.nil?
      redirect_to products_url
      return
    end

    if product.inventory <= 0
      flash[:notice] = "Product is out of stock, please contact seller!"
      redirect_to product_url(product)
      return
    end

    cart = Cart.find_or_create_by(user: current_user)
    unless cart.cart_items.nil?
      cart_item_for_prod = cart.cart_items.find {|cart_item| cart_item.product.id == product_id }
      unless cart_item_for_prod.nil?
        cart_item_for_prod.quantity += 1
        cart_item_for_prod.save
      else
        cart.cart_items << CartItem.create(cart: cart, product: product, quantity: 1)
      end
    else
      cart.cart_items = [CartItem.create(cart: cart, product: product, quantity: 1)]
    end

    if cart.save
      redirect_to cart_url
    else
      redirect_to product_url(product)
    end
  end

  def show
    @cart = Cart.find_or_create_by(user: current_user)
  end

  def destroy
    redirect_to cart_url
  end

  def batch_process
    cart = Cart.find_or_create_by(user: current_user)

    cart_item_ids = params[:ids].map {|cart_item_id| cart_item_id.to_i }
    unless cart_item_ids.nil? or cart_item_ids.length <= 0
      if params.has_key?("checkout-btn")
        cart_items = cart.cart_items.select {|cart_item| cart_item_ids.include?(cart_item.id) }
        if is_order_processable?(cart_items)
          order = Order.create(user: current_user, status: 'draft', total: 0)
          order_total = 0
          cart_items.each do |cart_item|
            order_total += cart_item.product.price * cart_item.quantity

            order_item = OrderItem.new
            order_item.title = cart_item.product.title
            order_item.description = cart_item.product.description
            order_item.image = cart_item.product.image.url
            order_item.price = cart_item.product.price
            order_item.quantity = cart_item.quantity
            order_item.order = order
            order_item.save

            cart_item.product.update(inventory: cart_item.product.inventory - cart_item.quantity)
          end
          cart_items.each {|cart_item| cart_item.destroy }

          order.update(total: order_total)
          redirect_to fill_order_shipping_address_url(order)

          return
        else
          flash[:notice] = 'No enough items available!'
        end
      else
        cart_item_ids.each do |cart_item_id|
          cart_item = cart.cart_items.find {|cart_item| cart_item.id == cart_item_id }
          unless cart_item.nil?
            cart_item.destroy
          end
        end
      end
    else
      flash[:notice] = "You must first select items to proceed!"
    end
    redirect_to cart_url
  end

  private
    def cart_params
      params.permit(:product_id, :cart_item_id)
    end

    def is_order_processable?(cart_items)
      cart_items.all? {|cart_item| has_enough_inventory?(cart_item) }
    end

    def has_enough_inventory?(cart_item)
      cart_item.product.inventory >= cart_item.quantity
    end
end
