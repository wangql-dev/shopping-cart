class CreateOrderItems < ActiveRecord::Migration[5.1]
  def change
    create_table :order_items do |t|
      t.string :title
      t.text :description
      t.string :image
      t.float :price
      t.integer :quantity
      t.references :order, foreign_key: true

      t.timestamps
    end
  end
end
