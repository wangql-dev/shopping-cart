class ProductsController < ApplicationController
  protect_from_forgery
  before_action :authenticate_user!, except: [:show, :index]
  before_action :is_seller?, except: [:show, :index]

  def index
  	@products = Product.all
  end

  def manage_products
    if user_signed_in? and current_user.seller?
      @products = Product.where(user: current_user)

      render 'admin_index'
    else
      redirect_to manage_products_url
    end
  end

  def show
    @product = Product.find_by(id: params[:id])
  end

  def new
  	@product = Product.new
  end

  def create
  	@product = Product.new(product_params)
  	@product.user = current_user
  	if @product.save
  	  redirect_to products_url
  	else
  	  render 'new'
  	end
  end

  def edit
  	@product = Product.find_or_create_by(id: params[:id], user: current_user)
  end

  def update
  	@product = Product.find_by(id: params[:id], user: current_user)
  	if @product.nil?
  	  redirect_to products_url
  	elsif @product.update(product_params)
  	  redirect_to products_url
  	else
  	  render 'edit'
  	end
  end

  def destroy
  	@product = Product.find_by(id: params[:id], user: current_user)
  	unless @product.nil?
  	  @product.destroy
  	end
  	redirect_to products_url
  end

  def is_seller?
    redirect_to root_url unless current_user.seller?
  end

  private
    def product_params
      params.require(:product).permit(:title, :description, :price, :inventory, :image, :image_cache, :remove_image)
    end
end
