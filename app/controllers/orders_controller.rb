class OrdersController < ApplicationController
  protect_from_forgery
  before_action :authenticate_user!

  def index
  	@orders = Order.where(user: current_user)
  end

  def manage_orders
  	@orders = Order.all
  	render 'admin_index'
  end

  def fill_shipping_address
  	@order = Order.find_by(id: params[:id], user: current_user)
  	if @order.nil? or @order.shipping_address.nil?
  	  @shipping_address = {}
  	else
  	  @shipping_address = JSON.parse @order.shipping_address
  	end
  end

  def save_shipping_address
  	shipping_address = params.require(:shipping_address).permit(:name, :address1, :address2, :city, :state, :country, :phone)

  	order = Order.find_by(id: params[:id], user: current_user)
  	unless order.nil?
  	  if order.status == 'draft'
  	    order.update(shipping_address: shipping_address.to_json, status: 'pending')
  	  else
  	    order.update(shipping_address: shipping_address.to_json)
  	  end
  	end

  	redirect_to orders_url
  end

  def update_status
	@order = Order.find_by(id: params[:id])
	if @order.nil?
	  redirect_to manage_orders_url
	  return
	end

  	if request.get?
	  @statuses = [['Draft', 'draft'], ['Paid', 'paid'], ['Pending', 'Pending'], ['Confirmed', 'confirmed'], ['Shipped', 'shipped'], ['Fulfilled', 'fulfilled'], ['Completed', 'completed'], ['Cancelled', 'cancelled']]
	  render 'update_status'
	else
	  @order.update(status: params[:status])
	  redirect_to manage_orders_url
	end
  end
end
