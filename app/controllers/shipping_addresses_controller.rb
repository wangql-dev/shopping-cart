class ShippingAddressesController < ApplicationController
  protect_from_forgery
  before_action :authenticate_user!

  def index
    @shipping_addresses = ShippingAddress.where(user: current_user)
  end

  def new
    @shipping_address = ShippingAddress.new
  end

  def create
    default_addr = current_user.shipping_addresses.find {|addr| addr.default }

    @shipping_address = ShippingAddress.new(shipping_address_params)
    @shipping_address.user = current_user
    if @shipping_address.save
      if @shipping_address.default
        default_addr.update(default: false)
      end
      redirect_to user_shipping_addresses_url
    else
      render 'new'
    end
  end

  def edit
    @shipping_address = ShippingAddress.find_by(id: params[:id], user: current_user)
  end

  def update
    @shipping_address = ShippingAddress.find_by(id: params[:id])
    if @shipping_address.nil?
      redirect_to user_shipping_addresses_url
    else
      default_addr = current_user.shipping_addresses.find {|addr| addr.default }
      if @shipping_address.update(shipping_address_params)
        if shipping_address_params[:default]
          default_addr.update(default: false) unless default_addr.nil?
        end
      else
        render 'edit'
      end
    end
  end

  def destroy
    @shipping_address = ShippingAddress.find_by(id: params[:id], user: current_user)
    unless @shipping_address.nil?
      @shipping_address.destroy
    end
    redirect_to user_shipping_addresses_url
  end

  private
    def shipping_address_params
      params.require(:shipping_address).permit(:name, :address1, :address2, :city, :state, :country, :phone, :default)
    end
end
