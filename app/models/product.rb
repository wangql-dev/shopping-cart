class Product < ApplicationRecord
  belongs_to :user

  mount_uploader :image, ProductImageUploader

  validates :title, presence: true
  validates :inventory, presence: true
  validates :price, presence: true
end
