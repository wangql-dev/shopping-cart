class AddDefaultToShippingAddress < ActiveRecord::Migration[5.1]
  def change
    add_column :shipping_addresses, :default, :boolean
  end
end
